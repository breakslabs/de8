DE-8: The 8-bit disassembler suite
----------------------------------

This project is intended to replace the earlier de64 decompiler suite, which
had grown too many warts before reaching a useful stage. Much of what I
considered while writing de64 have informed the overall design and structure
of this package - lessons learned and all that. As such, de8 is intended to be
a collection of disassembly and other "reversing" software for early 8-bit
architectures. The initial focus will be on the MOS 6502/6510 series CPUs, but
the design will allow for other platforms such as the Zilog Z80, the Motorola
6800 series, the Intel 8008/8080, &c.

Some changes between the two packages:


- More architecture independence. The de64 package started life as a simple
  6510/C-64 disassembler. Attempts were made late in the game to expand and
  abstract this model, which led to a mess.
- No unit tests. I've been giving them a fair shake for some years now, and I
  don't see the use in them for a project of this kind. The unit test LoC
  tended to exceed the actual application, and provided little benefit.
- No coverage reports - see above.
- Regression tests and system tests are likely when they make sense.
- Simpler command-line invocation. The de64 command-line syntax had become
  unwieldy and gross. I'm also a fan of dependency reduction, so this should
  remove the need for 'click' (no comment intended on the quality of 'click'
  itself, which gave me no serious issues).
- Leveraging the setuptools built-in plug-in infrastructure.
- Abstract Istruction Set Architecture design. De64 kind of stumbled into that
  accidentally, and I'd like to expand on that somewhat here.


