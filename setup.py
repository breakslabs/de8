#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from __future__ import absolute_import
from __future__ import print_function

import io
import re
from glob import glob
from os.path import basename
from os.path import dirname
from os.path import join
from os.path import splitext

from setuptools import find_packages
from setuptools import setup


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ).read()


setup(
    name='de8',
    version='0.0.5',
    license='GPLv3+',
    description='Tools for examining 8-bit assembly code and data',
    long_description='%s\n%s' % (
        re.compile('^.. start-badges.*^.. end-badges', re.M | re.S).sub('', read('README.rst')),
        re.sub(':[a-z]+:`~?(.*?)`', r'``\1``', read('CHANGELOG.org'))
    ),
    author='BS Labs',
    author_email='breakslabs@gmail.com',
    url='',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    include_package_data=True,
    zip_safe=False,
    classifiers=[ 
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Other Audience',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Assembly',
        'Topic :: Education',
        'Topic :: Software Development :: Disassemblers',
        'Topic :: Utilities',
    ],
    keywords=[
        '8-bit', 'disassembler', 'disassembly', '6502', '6510',
        '8008', '8080', 'z80',
    ],
    install_requires=[
        # eg:
        #    'Flask>=0.12',
    ],
    extras_require={
        # eg:
        #   'rst': ['docutils>=0.11'],
        #   ':python_version=="2.6"': ['argparse'],
    },
    entry_points={
        'console_scripts': [
            'de8 = de8.disassembler:main',
        ],
        'de8.cpu_plugin': [ 'mos6502 = de8.plug_ins.mos6502:MOS6502',
                            '6502 = de8.plug_ins.mos6502:MOS6502',
                            'mos6510 = de8.plug_ins.mos6502:MOS6502',
                            '6510 = de8.plug_ins.mos6502:MOS6502', ] ,
        'de8.platform_def':
        [ 'c64 = de8.plug_ins.mos6502:platform_c64',
          'commodore64 = de8.plug_ins.mos6502:platform_c64', ],
    },
)
