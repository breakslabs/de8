"""Command-line interface code for the de8 disassembler."""

import logging
log = logging.getLogger(__name__)
import argparse
import json
import math
import pathlib
import sys

import de8
from de8 import platform, cpu as de8cpu, memory, analysis, util


def print_list_and_exit(lst, indent='    ',exit_code=0):
    """Print the contents of `lst` to stdout and exit."""
    for i in lst:
        print(f'{indent}{i}')
    sys.exit(exit_code)

def parse_binaries(binaries):
    """Parse the list of binary load arguments from the command line."""
    bins = []
    for binary in binaries:
        vals = binary.split(':')
        if len(vals) > 4:
            raise SyntaxError(f'Invalid binary "{binary}".\nBinary format '
                              'is "<file>[:address[:offset[:count]]]"')
        path = pathlib.Path(vals[0])
        if not path.exists():
            raise FileNotFoundError(f'Can\'t find file "{path}"')
        rest = (int(v) for v in vals[1:]) if len(vals) > 1 else ()
        bins.append(de8.Binary(path, *rest))
    return bins

def parse_cmdline():
    """Parse the command line using argparse, and return the namespace."""
    # Be nice and be case-insensitive for CPU and platform names
    p = argparse.ArgumentParser(add_help=False)
    ns, remainder = de8.argparse_with_rcfile(p)
    cpus = util.uncased_list(de8.CPUs)
    platforms = util.uncased_list(de8.Platforms)
    cpus.append('list')
    platforms.append('list')
    p.add_argument('-C', '--cpu', type=str, default=None, choices=cpus,
                   help='CPU class. Default is the MOS-6502. Use "-c list" '
                   'to list known CPUs')
    p.add_argument('-p', '--platform', type=str, default=None,
                   choices=platforms,
                   help='Use a hardware platform definition. "-p list" to '
                   'list known definitions')
    ns, remainder = p.parse_known_args(remainder, namespace=ns)
    if ns.cpu == 'list':
        print('Available CPUs:')
        cpus.remove('list')
        print_list_and_exit(cpus)
    if ns.platform == 'list':
        print('Available platforms:')
        platforms.remove('list')
        print_list_and_exit(platforms)
    p.add_argument('binaries', type=str, action='append',
                   metavar='FILE', 
                   help='Load binary data from FILE into "RAM". The format is '
                   '<filename>[:address[:offset[:count]]]. Where address is '
                   'the base address to load the code into, offset is the '
                   'position within the file to start reading, and count is '
                   'the number of bytes to read. If address is ommitted, '
                   'behavior is dependent on the platform. If count is '
                   'omitted, the entire file is read, beginning at offset.'
                       ' This argument may be repeated')
    p.add_argument('-a', '--address', type=int, default=0,
                   help='Address to begin disassembly at')
    p.add_argument('-A', '--print_addresses', action='store_true',
                   help='Print instruction addresses in output.')
    p.add_argument('-r', '--radix', type=str, default='hex',
                   choices = [n.name.lower() for n in de8cpu.Radix],
                   help='Radix to use to display values')
    g = p.add_mutually_exclusive_group()
    g.add_argument('-l', '--limit-bytes', type=int, default=None,
                   metavar='COUNT', help='Stop disassembly after max COUNT '
                       'bytes')
    g.add_argument('-L', '--limit-instructions', type=int, default=None,
                   metavar='COUNT', help='Stop disassembly after max COUNT '
                       'instructions')
    p.add_argument('-I', '--limit-invalid', action='store_true',
                   help='Stop disassembly if an invalid opcode is encountered')
    p.add_argument('--help', '-h', action='help',
                   help='Show this message and exit')
    args = p.parse_args(remainder, namespace=ns)
    # The cost of being nice - get the case-sensitive value of the CPU or
    # platform arguments so registry look-ups work elsewhere.
    if args.cpu:
        if args.platform:
            p.error('argument -c/--cpu is not allowed with argument '
                    '-p/--platform')
        args.cpu = cpus.cased(args.cpu)
    elif args.platform:
        args.platform = platforms.cased(args.platform)
    else:
        p.error('one of the arguments -c/--cpu -p/--platform is required')
    return args

def main():
    args = parse_cmdline()
    try:
        bins = parse_binaries(args.binaries)
    except (SyntaxError, FileNotFoundError) as e:
        argparse.ArgumentParser.exit(1, f'{e}')
    if not args.platform:
        plat = platform.Platform.from_default_with_cpu(args.cpu)
    else:
        plat = platform.Platform.from_dict(de8.Platforms[args.platform])
    for b in bins:
        plat.load(*b)
    if args.limit_bytes is not None:
        limit = args.limit_bytes
        l_instrs = 'bytes'
    elif args.limit_instructions is not None:
        limit = args.limit_instructions
        l_instrs = 'instructions'
    else:
        limit = math.inf
        l_instrs = 'bytes'
    #dis = analysis.Disassembler(plat.cpu, plat.mmu)
    dis = plat.disassembler()
    dis.count_as = l_instrs
    dis.stop_on_invalid = args.limit_invalid
    
    labels = dict()
    labels[0x02c2]="PREDEFINED_LABEL"
    max_addr = dis.branch_map(labels, (args.address, limit))
    # Not permenant
    #out_of_scope = dict()
    #for k in labels:
    #    if not args.address <= k <= max_addr:
    #        out_of_scope[k] = labels[k]
    #for k in out_of_scope:
    #    labels.pop(k)
    print(f'org = ${args.address:04x}')   
    try:
        for instr in dis((args.address, limit)):
            (addr, mne, oper) = plat.cpu.format_instruction_hook(instr, labels,
                                            de8cpu.Radix.from_str(args.radix))
            label = labels.get(instr.address, '')
            #if len(label) < 9:
            #    print(f'{label:<9s} {addr:>5s}      {mne}    {oper}')
            #else:
            if label:
                print(f'{label}:')
            if args.print_addresses:
                print(f'          {addr:>5s}       {mne}    {oper}')
            else:
                print(f'          {mne}    {oper}')
    except memory.SegmentationFault:
        pass
    
    
