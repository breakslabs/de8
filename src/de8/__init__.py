from collections import namedtuple, abc
import json
import logging
import logging.config
import pathlib
import pkg_resources as _pr
import sys

logging.VERBOSE = 5
logging.addLevelName(logging.VERBOSE, 'VERBOSE')
def verbose(self, message, *args, **kws):
    if self.isEnabledFor(logging.VERBOSE):
        # Yes, logger takes its '*args' as 'args'.
        self._log(logging.VERBOSE, message, args, **kws) 
logging.Logger.verbose = verbose

logging.basicConfig()
log = logging.getLogger(__name__)

from .util import classproperty, DescriptorClass

def get_disabled(self):
    return getattr(self, '_disabled', False)

def set_disabled(self, disabled):
    if disabled:
        for x in [6, 5, 4, 3, 2]:
            fr = sys._getframe(x)
            print('{}:{}'.format(fr.f_code.co_filename, fr.f_lineno))
    frame = sys._getframe(1)
    if disabled:
        print('{}:{} disabled the {} logger'.format(
            frame.f_code.co_filename, frame.f_lineno, self.name))
    self._disabled = disabled

logging.Logger.disabled = property(get_disabled, set_disabled)


def config_logging(conf):
    cdict = conf.copy()
    cdict.setdefault('version', 1)
    cdict.setdefault('disable_existing_loggers', False)
    logging.config.dictConfig(cdict)
    log.debug(f'ROOT LOG CONFIG: {cdict}')


def argparse_with_rcfile(parser, args=None, namespace=None):
    """Given an existing ArgParse parser, handle rc file argument.

    :param parser: An existing argument parser instance.
    :type parser: :class:`argparse.ArgumentParser`
    :param list args: Optional list of arguments. If not provided or None, the
        command-line itself will be partially parsed.
    :return: Tuple containing the namespace and remaining, unparsed arguments.
    :rtype: list
    
    Given a parser, add a -c/--config option to the parser, then
    parse_known_args on the command line, or `args` if present. If a
    -c/--config option was present, load the JSON rc file it indicates,
    updating Platforms with user definitions if present, and place a `config`
    attribute on the namespace containing a dictionary of user configuration
    values. In short, the call signature and return values are identical to
    parse_known_args. This just handles the intermediate step so multiple
    command-line tools have a common interface.

    This utility requires that the given argument parser does not have or
    intend to have '-c/--config' or '-N/--no-rcfile' options of its own. This
    utility will immediately terminate the script with an error message if
    parsing fails (file not present, invalid JSON, etc.)
    """
    parser.add_argument('-c', '--config', type=pathlib.Path,
                        default=pathlib.Path('~/.de8rc'),
                        help='path to user RC file')
    parser.add_argument('-N', '--no-rcfile', action='store_true',
                        help='do not load any configuration file, even if '
                            'present')
    ns, remainder = parser.parse_known_args(args, namespace)
    if ns.no_rcfile:
        ns.config = {}
        return (ns, remainder)
    rcfile = ns.config.expanduser().resolve()
    try:
        with rcfile.open('r') as fp:
            ns.config = json.load(fp)
    except OSError as e:
        parser.error(f'Cannot load configuration file "{rcfile}": {e}')
    if 'platforms' in ns.config:
        Platforms.add_user_defs(ns.config['platforms'])
    return (ns, remainder)

class _PlugInIface(abc.Mapping):
    """Intarface for plug-in accessor."""
    def __init__(self, *, entry_point):
        self._entry_point = entry_point
        self._cache = {}
        super().__init__()

    def _ensure_cache(self):
        if not self._cache:
            self._cache = { ep.name: ep
                        for ep in _pr.iter_entry_points(self._entry_point) }
        
    @property
    def _values(self):
        self._ensure_cache()
        return self._cache
        
    def __getitem__(self, key):
        plugin = self._values.get(key, None)
        if plugin is None:
            raise KeyError(key)
        return plugin.load()

    def __setitem__(self, key, value):
        return NotImplemented

    def __delitem__(self, key):
        return NotImplemented

    def __len__(self):
        return len(self._values)

    def __iter__(self):
        return self._values.__iter__()

    def keys(self):
        return self._values.keys()

class _PlatPlugInIface(_PlugInIface):
    # This is sloppy in that it creates a new dict with copy on pretty much
    # every access. However, it's simpler and reduces code duplication this
    # way. Ignore it unless it becomes a problem.
    _user_defs = {}
    
    @property
    def _values(self):
        self._ensure_cache()
        rval = self._cache.copy()
        rval.update(self._user_defs)
        return rval
    
    def __getitem__(self, key):
        plugin = self._values.get(key, None)
        if plugin is None:
            raise KeyError(key)
        if isinstance(plugin, _pr.EntryPoint):
            plugin = plugin.load()
        return plugin

    def add_user_defs(self, platform_defs):
        self.__class__._user_defs.update(platform_defs)
        
    
CPUs = _PlugInIface(entry_point='de8.cpu_plugin')
Platforms = _PlatPlugInIface(entry_point='de8.platform_def')
    
Binary = namedtuple('Binary', ['path', 'address', 'offset', 'count'])
Binary.__doc__ = 'Container for a binary load target'
Binary.path.__doc__ = 'Path to the file containing the binary data'
Binary.address.__doc__ = 'Memory address at which to load the data'
Binary.offset.__doc__ = 'Offset within the file at which data begins'
Binary.count.__doc__ = 'Number of bytes to read into memory'
Binary.__new__.__defaults__ = (0, 0, -1)

    
