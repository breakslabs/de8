"""Miscellaneous functions and classes that don't fit elsewhere."""

""" Presently unused?

# Originally from StackOverflow user "Basj" at:
#     "https://stackoverflow.com/questions/3318625/"
#     "how-to-implement-an-efficient-bidirectional-hash-table"
# Efficient, effective, and simple implementation of a bi-directional hash.
class bidict(dict):
    def __init__(self, *args, **kwargs):
        super(bidict, self).__init__(*args, **kwargs)
        self.inverse = {}
        for key, value in self.items():
            self.inverse.setdefault(value,[]).append(key) 

    def __setitem__(self, key, value):
        if key in self:
            self.inverse[self[key]].remove(key) 
        super(bidict, self).__setitem__(key, value)
        self.inverse.setdefault(value,[]).append(key)        

    def __delitem__(self, key):
        self.inverse.setdefault(self[key],[]).remove(key)
        if self[key] in self.inverse and not self.inverse[self[key]]: 
            del self.inverse[self[key]]
        super(bidict, self).__delitem__(key)
"""

class uncased_list(list):
    """List subclass that is case-insensitive for string membership.

    Example:

    >>> ul = uncased_list(['a', 'b', 'C', 'D', 7, 21, None])
    >>> 'c' in ul
    True
    >>> 'C' in ul
    True
    >>> 'A' in ul
    True
    >>> 21 in ul
    True
    >>> 'pants' in ul
    False
    """
    def __contains__(self, key):
        if isinstance(key, str):
            return key.casefold() in [ i.casefold() for i in self
                                           if isinstance(i, str) ]
        else:
            return super().__contains__(key)
        
    def cased(self, key):
        """Return the cased value of item which matches `key`.

        Finds the first value in self which case-insensitively matches `key`
        and returns the cased version. If multiple matches exist, only the
        first is returned. If none match, None is returned.
        """
        match = [ i for i in self if i.casefold() == key.casefold() ]
        if len(match):
            return match[0]
        else:
            return None

# Descriptor class, metaclass, and decorator allowing class properties
# - like @property but for classes instead of instances.
# Used by the plug-in Registry, which is really just a namespace, so calling
# code doesn't need to instantiate it.
class ClassPropDescr:
    """Property descriptor class for classproperties."""
    def __init__(self, getter, setter=None):
        self.fget = getter
        self.fset = setter

    def __get__(self, obj, cls=None):
        # cls should never be None here, yes?
        # If cls is None, cls will become type(obj) which is DescriptorClass
        # That's not what we want.
        #if cls is None:
        #    cls = type(obj)
        return self.fget.__get__(obj, cls)()

    def __set__(self, obj, val):
        if not self.fset:
            raise AttributeError("can't set attribute")
        # cls = type(obj) # This is wrong. 
        return self.fset.__get__(None, obj)(val)

    def setter(self, func):
        if not isinstance(func, (classmethod, staticmethod)):
            func = classmethod(func)
        self.fset = func
        return self
    
def classproperty(func):
    if not isinstance(func, (classmethod, staticmethod)):
        func = classmethod(func)
    return ClassPropDescr(func)

class DescriptorClass(type):
    """Metaclass for classes which wish to use ClassPropDescr descriptors.

    Required so that __set__ functions correctly on the class instance.
    Non-data descriptor instances can probably function without this.
    """
    def __setattr__(self, key, val):
        obj = self.__dict__.get(key, None)
        if obj and type(obj) is ClassPropDescr:
            return obj.__set__(self, val)
        super().__setattr__(key, val)
        
