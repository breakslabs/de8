"""The MOS 6502/6510 CPU plug-in for de8.

This module provides a basic 8-bit CPU plug-in for the de8 disassembler
suite. It is written in conjunction with the de8 package, and is intended to
also serve as a template for other 8-bit CPU implementations.
"""
import enum

from de8 import cpu

class IdxReg(str, enum.Enum):
    REG_X = 'X'
    REG_Y = 'Y'

_BRANCHABLE = [
    cpu.AddrMode.ABS,
    cpu.AddrMode.ZPG,
    cpu.AddrMode.ABS_IDX,
    cpu.AddrMode.ZPG_IDX,
    cpu.AddrMode.IND_IDX,
    cpu.AddrMode.IDX_IND,
    ]
    
_opcodes_mos = {
    0: cpu.Opcode(0, 'BRK', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    1: cpu.Opcode(1, 'ORA', cpu.AddrMode.IDX_IND, cpu.T_UWORD_LE, False,
                  IdxReg.REG_X),
    5: cpu.Opcode(5, 'ORA', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    6: cpu.Opcode(6, 'ASL', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    8: cpu.Opcode(8, 'PHP', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    9: cpu.Opcode(9, 'ORA', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    10: cpu.Opcode(10, 'ASL', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    13: cpu.Opcode(13, 'ORA', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    14: cpu.Opcode(14, 'ASL', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    16: cpu.Opcode(16, 'BPL', cpu.AddrMode.REL, cpu.T_BYTE, True),
    17: cpu.Opcode(17, 'ORA', cpu.AddrMode.IND_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_Y),
    21: cpu.Opcode(21, 'ORA', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                   IdxReg.REG_X),
    22: cpu.Opcode(22, 'ASL', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                   IdxReg.REG_X),
    24: cpu.Opcode(24, 'CLC', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    25: cpu.Opcode(25, 'ORA', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_Y),
    29: cpu.Opcode(29, 'ORA', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_X),
    30: cpu.Opcode(30, 'ASL', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_X),
    32: cpu.Opcode(32, 'JSR', cpu.AddrMode.ABS, cpu.T_UWORD_LE, True),
    33: cpu.Opcode(33, 'AND', cpu.AddrMode.IDX_IND, cpu.T_UWORD_LE, False,
                   IdxReg.REG_X),
    36: cpu.Opcode(36, 'BIT', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    37: cpu.Opcode(37, 'AND', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    38: cpu.Opcode(38, 'ROL', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    40: cpu.Opcode(40, 'PLP', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    41: cpu.Opcode(41, 'AND', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    42: cpu.Opcode(42, 'ROL', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    44: cpu.Opcode(44, 'BIT', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    45: cpu.Opcode(45, 'AND', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    46: cpu.Opcode(46, 'ROL', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    48: cpu.Opcode(48, 'BMI', cpu.AddrMode.REL, cpu.T_BYTE, True),
    49: cpu.Opcode(49, 'AND', cpu.AddrMode.IND_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_Y),
    53: cpu.Opcode(53, 'AND', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                   IdxReg.REG_X),
    54: cpu.Opcode(54, 'ROL', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                   IdxReg.REG_X),
    56: cpu.Opcode(56, 'SEC', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    57: cpu.Opcode(57, 'AND', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_Y),
    61: cpu.Opcode(61, 'AND', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_X),
    62: cpu.Opcode(62, 'ROL', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_X),
    64: cpu.Opcode(64, 'RTI', cpu.AddrMode.IMPL, cpu.T_NONE, cpu.PC_POP),
    65: cpu.Opcode(65, 'EOR', cpu.AddrMode.IDX_IND, cpu.T_UWORD_LE, False,
                   IdxReg.REG_X),
    69: cpu.Opcode(69, 'EOR', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    70: cpu.Opcode(70, 'LSR', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    72: cpu.Opcode(72, 'PHA', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    73: cpu.Opcode(73, 'EOR', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    74: cpu.Opcode(74, 'LSR', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    76: cpu.Opcode(76, 'JMP', cpu.AddrMode.ABS, cpu.T_UWORD_LE, True),
    77: cpu.Opcode(77, 'EOR', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    78: cpu.Opcode(78, 'LSR', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    80: cpu.Opcode(80, 'BVC', cpu.AddrMode.REL, cpu.T_BYTE, False),
    81: cpu.Opcode(81, 'EOR', cpu.AddrMode.IND_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_Y),
    85: cpu.Opcode(85, 'EOR', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                   IdxReg.REG_X),
    86: cpu.Opcode(86, 'LSR', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                   IdxReg.REG_X),
    88: cpu.Opcode(88, 'CLI', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    89: cpu.Opcode(89, 'EOR', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_Y),
    93: cpu.Opcode(93, 'EOR', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_X),
    94: cpu.Opcode(94, 'LSR', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                   IdxReg.REG_X),
    96: cpu.Opcode(96, 'RTS', cpu.AddrMode.IMPL, cpu.T_NONE, cpu.PC_POP),
    97: cpu.Opcode(97, 'ADC', cpu.AddrMode.IDX_IND, cpu.T_UWORD_LE, False,
                   IdxReg.REG_X),
    101: cpu.Opcode(101, 'ADC', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    102: cpu.Opcode(102, 'ROR', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    104: cpu.Opcode(104, 'PLA', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    105: cpu.Opcode(105, 'ADC', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    106: cpu.Opcode(106, 'ROR', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    108: cpu.Opcode(108, 'JMP', cpu.AddrMode.IND, cpu.T_UWORD_LE, True),
    109: cpu.Opcode(109, 'ADC', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    110: cpu.Opcode(110, 'ROR', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    112: cpu.Opcode(112, 'BVS', cpu.AddrMode.REL, cpu.T_BYTE, False),
    113: cpu.Opcode(113, 'ADC', cpu.AddrMode.IND_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    117: cpu.Opcode(117, 'ADC', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_X),
    118: cpu.Opcode(118, 'ROR', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_X),
    120: cpu.Opcode(120, 'SEI', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    121: cpu.Opcode(121, 'ADC', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    125: cpu.Opcode(125, 'ADC', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    126: cpu.Opcode(126, 'ROR', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    129: cpu.Opcode(129, 'STA', cpu.AddrMode.IDX_IND, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    132: cpu.Opcode(132, 'STY', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    133: cpu.Opcode(133, 'STA', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    134: cpu.Opcode(134, 'STX', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    136: cpu.Opcode(136, 'DEY', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    138: cpu.Opcode(138, 'TXA', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    140: cpu.Opcode(140, 'STY', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    141: cpu.Opcode(141, 'STA', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    142: cpu.Opcode(142, 'STX', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    144: cpu.Opcode(144, 'BCC', cpu.AddrMode.REL, cpu.T_BYTE, True),
    145: cpu.Opcode(145, 'STA', cpu.AddrMode.IND_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    148: cpu.Opcode(148, 'STY', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_X),
    149: cpu.Opcode(149, 'STA', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_X),
    150: cpu.Opcode(150, 'STX', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_Y),
    152: cpu.Opcode(152, 'TYA', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    153: cpu.Opcode(153, 'STA', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    154: cpu.Opcode(154, 'TXS', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    157: cpu.Opcode(157, 'STA', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    160: cpu.Opcode(160, 'LDY', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    161: cpu.Opcode(161, 'LDA', cpu.AddrMode.IDX_IND, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    162: cpu.Opcode(162, 'LDX', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    164: cpu.Opcode(164, 'LDY', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    165: cpu.Opcode(165, 'LDA', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    166: cpu.Opcode(166, 'LDX', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    168: cpu.Opcode(168, 'TAY', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    169: cpu.Opcode(169, 'LDA', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    170: cpu.Opcode(170, 'TAX', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    172: cpu.Opcode(172, 'LDY', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    173: cpu.Opcode(173, 'LDA', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    174: cpu.Opcode(174, 'LDX', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    176: cpu.Opcode(176, 'BCS', cpu.AddrMode.REL, cpu.T_BYTE, True),
    177: cpu.Opcode(177, 'LDA', cpu.AddrMode.IND_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    180: cpu.Opcode(180, 'LDY', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_X),
    181: cpu.Opcode(181, 'LDA', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_X),
    182: cpu.Opcode(182, 'LDX', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_Y),
    184: cpu.Opcode(184, 'CLV', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    185: cpu.Opcode(185, 'LDA', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    186: cpu.Opcode(186, 'TSX', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    188: cpu.Opcode(188, 'LDY', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    189: cpu.Opcode(189, 'LDA', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    190: cpu.Opcode(190, 'LDX', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    192: cpu.Opcode(192, 'CPY', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    193: cpu.Opcode(193, 'CMP', cpu.AddrMode.IDX_IND, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    196: cpu.Opcode(196, 'CPY', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    197: cpu.Opcode(197, 'CMP', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    198: cpu.Opcode(198, 'DEC', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    200: cpu.Opcode(200, 'INY', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    201: cpu.Opcode(201, 'CMP', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    202: cpu.Opcode(202, 'DEX', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    204: cpu.Opcode(204, 'CPY', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    205: cpu.Opcode(205, 'CMP', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    206: cpu.Opcode(206, 'DEC', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    208: cpu.Opcode(208, 'BNE', cpu.AddrMode.REL, cpu.T_BYTE, True),
    209: cpu.Opcode(209, 'CMP', cpu.AddrMode.IND_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    213: cpu.Opcode(213, 'CMP', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_X),
    214: cpu.Opcode(214, 'DEC', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_X),
    216: cpu.Opcode(216, 'CLD', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    217: cpu.Opcode(217, 'CMP', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    221: cpu.Opcode(221, 'CMP', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    222: cpu.Opcode(222, 'DEC', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    224: cpu.Opcode(224, 'CPX', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    225: cpu.Opcode(225, 'SBC', cpu.AddrMode.IDX_IND, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    228: cpu.Opcode(228, 'CPX', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    229: cpu.Opcode(229, 'SBC', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    230: cpu.Opcode(230, 'INC', cpu.AddrMode.ZPG, cpu.T_UBYTE, False),
    232: cpu.Opcode(232, 'INX', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    233: cpu.Opcode(233, 'SBC', cpu.AddrMode.IMM, cpu.T_UBYTE, False),
    234: cpu.Opcode(234, 'NOP', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    236: cpu.Opcode(236, 'CPX', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    237: cpu.Opcode(237, 'SBC', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    238: cpu.Opcode(238, 'INC', cpu.AddrMode.ABS, cpu.T_UWORD_LE, False),
    240: cpu.Opcode(240, 'BEQ', cpu.AddrMode.REL, cpu.T_BYTE, True),
    241: cpu.Opcode(241, 'SBC', cpu.AddrMode.IND_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    245: cpu.Opcode(245, 'SBC', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_X),
    246: cpu.Opcode(246, 'INC', cpu.AddrMode.ZPG_IDX, cpu.T_UBYTE, False,
                    IdxReg.REG_X),
    248: cpu.Opcode(248, 'SED', cpu.AddrMode.IMPL, cpu.T_NONE, False),
    249: cpu.Opcode(249, 'SBC', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_Y),
    253: cpu.Opcode(253, 'SBC', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
    254: cpu.Opcode(254, 'INC', cpu.AddrMode.ABS_IDX, cpu.T_UWORD_LE, False,
                    IdxReg.REG_X),
}

    
class MOS6502(cpu.CPU):
    """MOS 6502 CPU implementation - disassemble only.

    Sample (usable) CPU implementation. The simplest CPU implemantation is a
    class which is a child of :class:`de8.cpu.CPU`, with an `opcodes`
    property. The `opcodes` property may be any container which maps an
    integer opcode value to an :class:`~de8.cpu.Opcode` instance. This
    imlementation just uses a dictionary, but more complex setups are
    possible.

    The :class:`~de8.cpu.CPU` class has other hooks and properties which may
    be implemented if desired to modify output formatting, branch
    identification, &c. See the :class:`de8.cpu.CPU` class documentation for
    more details.
    """
    opcodes = _opcodes_mos
    _radix_fmts_b = { cpu.Radix.BIN: '{s}%{v:08b}',
                          cpu.Radix.OCT: '{s}&{v:03o}',
                          cpu.Radix.DEC: '{s}{v}',
                          cpu.Radix.HEX: '{s}${v:02X}',
                          }
    _radix_fmts_w = { cpu.Radix.BIN: '{s}%{v:16b}',
                          cpu.Radix.OCT: '{s}&{v:06o}',
                          cpu.Radix.DEC: '{s}{v}',
                          cpu.Radix.HEX: '{s}${v:04X}',
                          }
    _radix_fmts_a = { cpu.Radix.BIN: '${v:04X}',
                          cpu.Radix.OCT: '&{v:06o}',
                          cpu.Radix.DEC: '{v:05d}',
                          cpu.Radix.HEX: '${v:04X}',
                          }
    _operand_fmt = { cpu.AddrMode.IND: '({addr})',
                         cpu.AddrMode.ABS_IDX: '{addr},{reg}',
                         cpu.AddrMode.ZPG_IDX: '{addr},{reg}',
                         cpu.AddrMode.IDX_IND: '({addr},{reg})',
                         cpu.AddrMode.IND_IDX: '({addr}),{reg}',
            }
    def format_instruction_hook(self, instr, labels={}, radix=cpu.Radix.HEX):
        """Given an :class:`~de8.cpu.Instruction` return a formatting triplet.

        :param int addr: The current memory address.
        :param instr: The instruction to format.
        :param dict labels: Address to label map. If omitted or empty, no
            labels are used.
        :param radix: The desired radix for operands (and optionally
            addresses). 
        :type instr: :class:`de8.cpu.Instruction`
        :type radix: :class:`de8.cpu.Radix`
        :return: A tuple containing three format strings. The first for the
            address, the second for the opcode mnemonic, and the third for the
            operand.
        :rtype: tuple (str, str, str)
        """
        label = labels.get(instr.address, '')
        if instr.opcode.addr_mode in _BRANCHABLE:
            label = labels.get(instr.operand, None)
        elif instr.opcode.addr_mode is cpu.AddrMode.REL:
            dest = self.get_relative_address(instr)
            label = labels.get(dest, None)
        else:
            label = None

        if instr.operand is not None and label is None:
            sign = '-' if instr.operand < 0 else ''
            if instr.opcode.oper_type == cpu.T_WORD_LE:
                operand_str = self._radix_fmts_w[radix].format(
                    s=sign, v=abs(instr.operand))
            else:
                operand_str = self._radix_fmts_b[radix].format(
                    s=sign, v=abs(instr.operand))
        else:
            operand_str = label if label is not None else ''
        # No instruction addresses in binary. Just... no.
        addr_str = self._radix_fmts_a[radix].format(v=instr.address)
        if instr.opcode.addr_mode in self._operand_fmt:
            reg = instr.opcode.data
            fmt = self._operand_fmt[instr.opcode.addr_mode]
            operand_str = fmt.format(addr=operand_str, reg=reg)
        return (addr_str, instr.opcode.mne, operand_str)

# Commodore-64 platform definition
platform_c64 = {
    'cpu': 'mos6510',
    'memory_size': '64k',
    'memory_maps': [],
    }
    
