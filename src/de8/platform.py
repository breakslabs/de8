"""Platform stub"""
import json
import math
import pathlib

import de8
from de8 import memory, analysis


class Platform:
    def __init__(self, cpu, mmu=memory.MemoryManager):
        self.mmu = mmu()
        self._last_prio = 0
        self.cpu = cpu
        
    def load(self, path, address, offset=0, count=-1):
        path = pathlib.Path(path)
        with path.open('rb') as f:
            if offset > 0:
                f.read(offset)
            data = f.read(count)
        seg =  memory.Memory(address, data)
        self.mmu.add_segment(seg, self._last_prio)
        self._last_prio += 1

    def disassembler(self):
        """Return a disassembler configured from this platform.

        Returns a disassembler configured to use the CPU and memory defined by
        this platform instance.
        """
        return analysis.Disassembler(self.cpu, self.mmu)
        
    @classmethod
    def from_json(cls, json_conf):
        """Return a Platform instance configured from JSON data.
        
        :param json_conf: The JSON data which contains this platform
            configuration. 
        :type json_conf: str or file-like object
        :return: Configured Platform instance
        :rtype: :class:`Platform`
        :raises json.decoder.JSONDecodeError: on malformed JSON data.

        The JSON data root object must be a either a dict containing the
        configuration values, or a dict with a 'de8_platform' key whose value
        is a dict containing the configuration values. If the latter is
        present, any configuration values outside of its container will be
        ignored. Additionally, any unknown keys will be ignored. This allows
        the configuration to be contained as part of another document if
        desired.
        """
        if hasattr(json.read):
            conf = json.load(json_conf)
        else:
            conf = json.loads(json_conf)
        if not isinstance(conf, dict):
            raise ValueError('JSON configuration root object must be a dict')
        conf = conf.get('de8_platform', conf)
        return cls.from_dict(conf)

    @classmethod
    def from_dict(cls, conf):
        """Return a configured Platform instance given a configuration dict.
        
        :param dict conf: A dictionary containing configuration values for the
            desired Platform instance.
        :return: A configured Platform instance.
        :rtype: :class:`Platform`
        :raises ValueError: on missing or invalid configuration data.

        *** Put configuration dict schema here once we know what it is***
        """
        if not 'cpu' in conf:
            raise ValueError('platform configuration must contain a CPU '
                             'designation')
        cpu = de8.CPUs[conf['cpu']]
        return cls(cpu())

    @classmethod
    def from_default_with_cpu(cls, cpu):
        """Return a default Platform instance with the given CPU.

        :param cpu: The CPU instance to assign to the platform.
        :type CPU: :class:`de8.cpu.CPU`
        :return: A configured platform instance with the given CPU.
        :rtype: :class:`Platform`
        """
        return cls.from_dict({'cpu': cpu})
        
