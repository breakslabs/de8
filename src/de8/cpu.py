from collections import namedtuple
import enum
import math
import re
import struct

from de8.memory import SegmentationFault

class CPUException(Exception):
    pass

class InvalidOpcode(CPUException):
    pass

class BoundsError(CPUException):
    pass

class PC_POP:
    """'Type-only' class for instructions which branch by popping the stack.

    Intended as the `non_seq` value for Opcodes like RTS and RTI. These
    instructions alter the program counter in a non-sequential way, but do so
    by popping the stack into the PC. Do not instantiate the value,
    just assign to the class. e.g.:
        Opcode(96, 'RTS', ..., ..., PC_POP, 1)
    """

class AddrMode(int, enum.Enum):
    """Possible address modes for CPU instructions.

    This attempts to cover all useful cases for common 8-bit
    microprocessors. If any are missing, please open an issue
    request. Alternate naming is also possible where reasonable.
    """
    NONE = enum.auto()        # Used for invalid opcodes
    IMPL = enum.auto()        # Implied
    ACCUM = IMPL              # Accumulator
    SEQ = IMPL                # Sequential
    IMM = enum.auto()         # Immediate
    ABS = enum.auto()         # Absolute (Direct)
    DIR = ABS
    IND = enum.auto()         # Indirect
    REL = enum.auto()         # Relative
    REG = enum.auto()         # Register
    ABS_IDX = enum.auto()     # Absolute Indexed
    IND_IDX = enum.auto()     # Indirect Indexed
    IDX_IND = enum.auto()     # Indexed Indirect
    SKIP = enum.auto()        # Skip
    ZPG = enum.auto()        # Zero page
    ZPG_IDX = enum.auto()    # Zero page Indexed

class Radix(int, enum.Enum):
    """Radix enumeration with flag value equal to the radix value."""
    BIN = 2
    OCT = 8
    DEC = 10
    HEX = 16

    @classmethod
    def from_str(cls, str):
        return getattr(cls, str.upper(), None)

class DataType(struct.Struct):
    """Helper class for operand data types.

    CPU implementations can use this class to define their data types using
    the struct format available from the stdlib `struct` module. See
    :mod:`mos6502` for a sample.

    If this is not sufficient for your implementation, feel free to create
    your own data type class. See below.

    Duck type for operand data types is:
       - must have a 'size' attribute that returns the number of bytes
         required to hold this data type.
       - Must be callable, accepting a byte string and an optional offset, and
         returning the value encoded in the byte string at the offset.
    """
    def __call__(self, string, offset=0):
        if self.format:
            return self.unpack_from(string, offset)[0]
        return None

T_UBYTE = DataType('B') # Unsigned byte
T_BYTE = DataType('b')  # Signed byte
T_UWORD_LE = DataType('<H') # Unsigned 16-bit word
T_UWORD_BE = DataType('>H') # Unsigned 16-bit word
T_WORD_LE = DataType('<h')  # Signed 16-bit word
T_WORD_BE = DataType('>h')  # Signed 16-bit word
T_NONE = DataType('')   # No operand

Opcode = namedtuple('Opcode', ['val', 'mne', 'addr_mode', 'oper_type',
                                   'non_seq', 'data'])
Opcode.__new__.__defaults__ = (False, None,)
Opcode.__doc__ = "Opcode definition class"
Opcode.val.__doc__ = "Opcode's numeric value"
Opcode.mne.__doc__ = "Opcode's mnemonic, as a string"
Opcode.addr_mode.__doc__ = "Opcode's addressing mode (:class:`AddrMode`)"
Opcode.oper_type.__doc__ = "Opcode's operand data type (:class:`DataType`)"
Opcode.non_seq.__doc__ = ("(OPT) If not None or False, indicates the "
                             "instruction branches. May also be a callable "
                             "which should return an (base) address, None if "
                             "incalculable, or True for pop-stack (e.g. RTS)")
Opcode.data.__doc__ = ("(OPT) Extra data not required by the disassembler. "
                           "This may be used by CPU implementations "
                           "(e.g. for identifying affected registers).")

Instruction = namedtuple('Instruction', ['address', 'opcode', 'operand',])
Instruction.__len__ = lambda self: (self.opcode.oper_type.size
                                        if self.opcode.oper_type else 0) + 1

class CPU:
    """Presently an over-engineered interface to the opcode table"""
    INVALID_OPCODE = Opcode(-1, '---', AddrMode.NONE, None, False)
    
#    def __init__(self, opcodes):
#        self.opcodes = opcodes

    def __getitem__(self, key):
        if not key in self.opcodes:
            raise InvalidOpcode(f'{key}')
        return self.opcodes[key]

    def get_instr_at(self, memory, address=0):
        """Return instruction at given address and number of bytes consumed.

        :param list memory: Indexible to read from.
        :param int address: Position in index to read at.
        :return: Tuple containing the Instruction at `address` and its operand
            if any, and the total number of bytes read.
        :rtype: tuple (:class:`Instruction`, int)
        :raises SegmentationFault: If parsing instruction requires reading
            past end of `memory`.
        """
        pc = address
        try:
            opcode = self[memory[pc]]
        except InvalidOpcode:
            opcode = self.INVALID_OPCODE
        except IndexError as e: # Deal with non-memory type iterables
            raise SegmentationFault from e
        finally:
            pc += 1
        if opcode.oper_type:
            try:
                operand = opcode.oper_type(
                    bytes(memory[pc:pc+opcode.oper_type.size]))
            except IndexError as e: # Same as above
                raise SegmentationFault from e
            finally:
                pc += opcode.oper_type.size
        else:
            operand = None
        return Instruction(address, opcode, operand)

    def iter_instructions(self, mem, address=0):
        """Yield instructions from an indexible iterable.

        :param list memory: The iterable containing the data to be 
            disassembled.
        :param int address: The base address to use as the starting point of
            disassembly. This is *not* added to anything, and is simply used
            to return the instruction address, which will be 'address' + PC.
        :return: A generator which produces :class:`Instruction`s
        :rtype: generator
        :raises SegmentationFault: If disassembly exceeds the boundaries of 
            the passed memory object.
        """
        pc = address
        while True:
            instr = self.get_instr_at(mem, pc)
            next_addr = yield instr
            pc += len(instr) if next_addr is None else next_addr

    def get_relative_address(self, instr):
        """Return the relative operand address of an AddrMode.REL instruction.
        
        :param object instr: The :class:`Instruction`
        :return: The absolute address referred to by the operand
        :rtype: int
        """
        assert instr.opcode.addr_mode is AddrMode.REL
        return instr.address + len(instr) + instr.operand
            

    
