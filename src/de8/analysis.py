"""Disassemblers, hex dumpers, branch and code analysis, &c."""
import re

from de8 import memory
from de8.cpu import AddrMode

class Disassembler:
    """Base disassembler class - useful for most applications.

    May be sub-classed and attached to a CPU child via the CPU's
    :attr:`disassembler_class` attribute.
    """

    def __init__(self, cpu, memory):
        """Initialize disassembler.
    
        :param object cpu: The CPU instance this disassembler belongs to.
        :param memory: An array of bytes containing the instructions to
            disassemble. We attempt to accept any array-like object, but a
            :class:'~de8.memory.MemoryManager` instance is preferred where
            possible.
        """
        self.cpu = cpu
        self._memory = memory
        self.count_as = 'instrs'
        self.stop_on_invalid = False

    @property
    def count_as(self):
        """Whether to treat `length` arguments to method calls as bytes or
        instructions (i.e. setting this property to 'bytes' then calling the
        :meth:`disassemble` method as "dis.disassemble(0, 40)" will stop
        disassembly after 40 bytes). May be changed at will, but doing so in
        the middle of disassembly may cause undesirable behavior.
        """
        return self._count_as

    @count_as.setter
    def count_as(self, val):
        val = val.lower()
        assert val in ['instr', 'instructions', 'bytes', 'instrs', 'i', 'b', ]
        if val in ['instr', 'instructions', 'instrs', 'i']:
            self._count_as = 'instructions'
        else:
            self._count_as = 'bytes'

    def _find_label_indices(self, labels, mem_map):
        """Find existing max indices in a mapping for a list of labels.

        :param list labels: List of label names.
        :param dict mem_map: An existing memory map dictionary, containing
            labels and their associated memory addresses.
        :return: Dictionary containing one entry for each name in `labels`
            the labels highest index as it appears in memory map as its
            value.

        Used by the automatic branch mapper to continue where it left
        off. E.g. if the memory map has "LABEL1" and "LABEL2" in it, and we
        passed 'LABEL' as one of the items in `labels`, the returned dict will
        contain {'LABEL': 2}. If no instance of 'LABELx' is encountered, the
        resulting dict will contain {'LABEL': 0}.
        """
        lmap = dict()
        for lab in labels:
            lab_re = re.compile(f'{lab}([0-9])*')
            found =[ int(lab_re.match(k).group(1)) for k in mem_map.values()
                     if lab_re.match(k) ]
            lmap[lab] = max(found) if len(found) else 0
        return lmap

    def _iter_until_segfault(self, gen):
        """Wrapper to convert SegmentationFault to StopIteration."""
        while True:
            try:
                yield from gen
            except memory.SegmentationFault:
                break

    def _iter_for_count(self, gen, count):
        """Wrapper to iterate instructions until `count`.

        :param generator gen: Generator or other iterable of
            :class:`~de8.cpu.Instruiction`s.
        :param int count: The number of bytes or instructions (see
            :attr:`count_as`) to iterate.
        """
        idx = 0
        count_bytes = (self.count_as == 'bytes')
        for instr in gen:
            if self.stop_on_invalid and instr.opcode.val < 0:
                break
            yield instr
            idx += len(instr) if count_bytes else 1
            if idx >= count:
                break

    def __call__(self, span):
        """Disassemble a range of memory.
        
        :param tuple span: Tuple containing start address and count.
        :return: Yields :class:`~de8.cpu.Instruction`s
        :rtype: :class:`de8.cpu.Instruction`
        """
        source = self.cpu.iter_instructions(self._memory, span[0])
        segfault_guard = self._iter_until_segfault(source)
        instructions = self._iter_for_count(segfault_guard, span[1])
        for instr in instructions:
            yield instr
            
    def branch_map(self, mem_map, source_span, dest_span=None):
        """Create a branch map for a given area of memory.

        :param dict mem_map: Dictionary mapping addresses to labels. Values
            here take precedence over calculated values - `branch_pass` will
            add to this map, but never overwrite existing values in it. The
            dictionary is modified in place, not copied.
        :param tuple source_span: Tuple containing the start addresses
            and number of bytes (or instructions, see :attr:`count_as`) to
            examine. Examination may end prematurely if
            :attr:`stop_at_invalid` is True and an invalid instruction is
            encountered.
        :param tuple dest_span: Optional tuple defining target memory:
            (start_address, end_address). If omitted or None, the allowed
            range is identical to the examination range. Numbers outside the
            range of actual memory are acceptable (e.g. (0, math.inf)).
        :return: The address of the last instruction examined.
        :rtype: int

        Examines the instructions in `source_span` for branches (JMP, JSR,
        BNE, &c). For each branch found for which a destination address can be
        determined, a generic label is added to `mem_map` if a) The
        destination address falls within the range given by `dest_span`, and
        b) a label for that address does not already exist in `mem_map`.

        TL;DR - Automatically generates address labels for probable routines
        and loop points.
        """
        address, count = source_span
        if dest_span is None:
            for instr in self._iter_for_count(self._iter_until_segfault(
                self.cpu.iter_instructions(self._memory, address)), count):
                max_address = instr.address
            dest_span = (address, max_address)
        idx = self._find_label_indices(['LABEL', 'TABLE',], mem_map)
        max_address = address
        source = self.cpu.iter_instructions(self._memory, address)
        instructions = self._iter_until_segfault(source)
        for instr in self._iter_for_count(instructions, count):
            if not instr.opcode.non_seq:
                continue
            if callable(instr.opcode.non_seq):
                dest = instr.opcode.non_seq(instr)
                if dest in (None, False):
                    continue
                tag='LABEL'
            else:
                if instr.opcode.addr_mode in [AddrMode.ABS, AddrMode.ZPG,
                                                  AddrMode.ABS_IDX,
                                                  AddrMode.ZPG_IDX]:
                    dest = instr.operand
                    tag = 'LABEL'
                elif instr.opcode.addr_mode in [AddrMode.IND_IDX,
                                                    AddrMode.IDX_IND]:
                    dest = instr.operand
                    tag = 'TABLE'
                elif instr.opcode.addr_mode is AddrMode.REL:
                    dest = self.cpu.get_relative_address(instr)
                    tag = 'LABEL'
                else:
                    raise NotImplementedError('non-sequential addr mode '
                                              f'"{opcode.addr_mode.name}"')
            if dest_span[0] <= dest <= dest_span[1]:
                mem_map.setdefault(dest, f'{tag}_{idx[tag]}')
                idx[tag] += 1
            max_address = instr.address
        return max_address

    
